$(document).ready(function () {

$('.delete').on('click', function (e) {
	e.preventDefault();
	if(confirm('¿Está seguro que quiere borrar este elemento?')) {
        var id = $(this).data("id");
		var model = $(this).data("model");
        var csrf_token = $("#csrf_token").val();
        $.ajax(
        {
            url: model+"/"+id,
            type: 'DELETE',
            dataType: "JSON",
            data: {
                "id": id,
                "_method": 'DELETE',
                "_token": csrf_token,
            },
            success: function (r)
            {
				document.location.href=document.location.href;
            },
			error: function (e,f)
			{
				console.error(e,f);
			}
        });		
	} 	
})
	
	
	
});

<?php

use Illuminate\Database\Seeder;

class GruposRolesSeeder extends Seeder
{
    /**s
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grupos')->insert([
            'name' => 'Test de grupo',
            'description' => 'Lorem ipsum',
			'created_at' => DB::raw('CURRENT_TIMESTAMP'),
			'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
		
        DB::table('roles')->insert([
            'name' => 'Test de rol',
            'description' => 'Lorem ipsum',
			'created_at' => DB::raw('CURRENT_TIMESTAMP'),
			'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);		
    }
}

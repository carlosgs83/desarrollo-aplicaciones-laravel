<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');			
            $table->string('email')->unique();
            $table->integer('visits');
            $table->boolean('is_available');
			$table->enum('gender', ['male', 'female']);	
            $table->timestamps();

			$table->index(['visits']);				
		});
	}
	
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}

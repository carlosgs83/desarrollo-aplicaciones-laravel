<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasGruposRolesRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		/**
		 * Relación 1:N entre personas y grupos. Modificamos tabla.
		 * El campo que lo relaciona es grupo_id en la tabla personas
		 * Establecemos una relación de foreign key 
		 * entre grupo_id e id de la tabla grupos
		 */
		Schema::table('personas', function (Blueprint $table) {
			$table->integer('grupo_id')->unsigned()->nullable();
			$table->foreign('grupo_id')->references('id')->on('grupos')->onDelete('set null');
		});
	
		/**
		 * Relación M:N entre personas y roles
		 * Creamos una tabla pivor con foreign keys a personas y roles
		 */
		Schema::create('persona_pivot_rol', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('persona_id')->unsigned();
			$table->foreign('persona_id')->references('id')->on('personas')->onDelete('cascade');
			$table->integer('rol_id')->unsigned();
			$table->foreign('rol_id')->references('id')->on('roles')->onDelete('cascade');
			
			$table->timestamps();
		 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		// Eliminamos el campo grupo_id y la clave foránea
		Schema::table('personas', function(Blueprint $table) {
			$table->dropForeign(['grupo_id']);
			$table->dropColumn('grupo_id');
		});
		
		// Eliminamos la tabla pivot
        Schema::dropIfExists('persona_pivot_rol');
    }
}

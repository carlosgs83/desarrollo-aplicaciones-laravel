@extends('layouts.main') 

@section('content')
<input type="hidden" id="csrf_token" value="{{ csrf_token() }}"/>
<div class="container mt-2">
	<h1>{{ $title }}</h1>
	<div class="row mt-5">
		<div class="col">
			<a href="/personas/create">Añadir nuevo elemento</a>
		</div>
	</div>

@if(count($personas)>0)
	<div class="row mt-5">
		<div class="col">
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th scope="col">ID</th>
						@foreach($attributes as $attr)
						<th scope="col">{{ $attr }}</th>
						@endforeach
						<th scope="col">&nbsp;</th>
						<th scope="col">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
				@foreach($personas as $persona)
					<tr>
						<th scope="row">
							<a href="/personas/{{ $persona->id }}">
								{{ $persona->id }}
							</a>
						</th>
						<td>{{ $persona->name }}</td>
						<td>{{ $persona->surname }}</td>
						<td>
							<a href="mailto:{{ $persona->email }}">
								{{ $persona->email }}
							</a>
						</td>
						<td>{{ $persona->visits }}</td>
						<td>
						@if($persona->is_available)
							true
						@else
							false
						@endif
						</td>
						<td>{{ $persona->gender }}</td>
						<td>
						@if(is_numeric($persona->grupo_id))
							<a href="/grupos/{{ $persona->grupo_id }}" target="_blank">
						@endif
							{{ $persona->grupo_name }}
						@if(is_numeric($persona->grupo_id))
							</a>
						@endif
						</td>
						<td>
						@if(count($persona->roles)>0)
							@foreach($persona->roles as $rol)
								<a href="/roles/{{ $rol->id }}" target="_blank">
								{{ $rol->name }}
								</a>
							@if($loop->count > 1 && !$loop->last)
							,
							@endif
							@endforeach
						@endif
						</td>	  
						<td>{{ $persona->created_at }}</td>
						<td>{{ $persona->updated_at }}</td>
						<td>
							<a href="/personas/{{ $persona->id }}/edit">Editar</a>
						</td>
						<td>
							<a href="#" class="delete" data-model="personas" data-id="{{ $persona->id }}">Borrar</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $personas->links() }}
		</div>

	</div>
@else
	<div class="row mt-5">
		<div class="col">
			<div class="alert alert-danger alert-dismissible">
				<strong>No hay elementos</strong> 
				Puede añadir un nuevo elemento desde el enlace superior.
			</div>
		</div>
	</div>
@endif

</div>
@stop 
@extends('layouts.main') 

@section('content')
<div class="container mt-2">
	<h1>{{ $title }}</h1>

	@if ($errors->any())
	<div class="row mt-4">
		<div class="col">	
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	@endif

	<div class="row mt-3">
		<div class="col">	

			<div class="form-group">
				<label for="name">
					<strong>ID:</strong>
				</label>
				<span> {{ $values['id'] }}  </span>
			</div>

			<div class="form-group">
				<label for="name">
					<strong>Nombre:</strong>
				</label>
				<span> {{ $values['name'] }}  </span>
			</div>
			<div class="form-group">
				<label for="surname">
					<strong>Apellidos:</strong>
				</label>
				<span> {{ $values['surname'] }}  </span>
			</div>
			<div class="form-group">
				<label for="email">
					<strong>Email:</strong>
				</label>
				<span> <a href="mailto:{{ $values['email'] }}">{{ $values['email'] }}</a>  </span>
			</div>
			<div class="form-group">
				<label for="visits">
					<strong>Visitas:</strong>
				</label>
				<span> {{ $values['visits'] }}  </span>
			</div>
			
			<div class="form-group">
				<label class="form-check-label" for="is_available">
					<strong>Disponible:</strong>
				</label>
				<span> @if($values['is_available']) true @else false @endif  </span>
			</div>

			<div class="form-group">
				<label for="gender">
					<strong>Género:</strong>
				</label>
				<span> 
					@if($values['gender']=='male') male 
					@elseif($values['gender']=='female') female 
					@endif  
				</span>					
			</div>			

			<div class="form-group">
				<label for="grupo_id">
					<strong>Grupo:</strong>
				</label>
				<span>
					@if(is_numeric($values['grupo_id']))
					<a href="/grupos/{{ $values['grupo_id'] }}" target="_blank"> 
					@endif
						{{ $values['grupo_name'] }}
					@if(is_numeric($values['grupo_id']))
					</a>
					@endif
				</span>					
			</div>		

			<div class="form-group">
				<label for="grupo_id">
					<strong>Roles:</strong>
				</label>
				<br/>
				@if(count($values['roles'])>0)
				<ul>
					@foreach($values['roles'] as $rol)
					<li>
						<a href="/roles/{{ $rol->id }}" target="_blank">{{ $rol->name }}</a>
					</li>
					@endforeach
				</ul>
				@else
					<span>No hay roles asociados</span>
				@endif				
			</div>			
			
			<div class="form-group">
				<label for="name">
					<strong>Created at:</strong>
				</label>
				<span> {{ $values['created_at'] }}  </span>
			</div>
			<div class="form-group">
				<label for="name">
					<strong>Last update:</strong>
				</label>
				<span> {{ $values['updated_at'] }}  </span>
			</div>  

		</div>
	</div>

	<div class="row mt-2">
		<div class="col">
			<a href="/personas">Volver</a>
		</div>
	</div>	
	
</div>
@stop 
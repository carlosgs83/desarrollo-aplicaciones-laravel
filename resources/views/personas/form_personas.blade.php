@extends('layouts.main') 

@section('content')
<div class="container mt-2">
	<h1>{{ $title }}</h1>

	@if ($errors->any())
	<div class="row mt-4">
		<div class="col">	
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	@endif

	<div class="row mt-3">
		<div class="col">	
			<form action="{{ $values['action'] }}" method="post">
				@csrf

				@if(is_numeric($values['id']))
				{{ method_field('PATCH') }}
				@endif

				@if(is_numeric($values['id']))
				<div class="form-group">
					<label for="name">
						<strong>ID:</strong>
					</label>
					<span> {{ $values['id'] }}  </span>
				</div>
				@endif

				<div class="form-group">
					<label for="name">
						<strong>Nombre:</strong>
					</label>
					<input type="text" class="form-control" name="name" id="name" placeholder="Nombre" value="{{ $values['name'] }}">
				</div>
				<div class="form-group">
					<label for="surname">
						<strong>Apellidos:</strong>
					</label>
					<input type="text" class="form-control" name="surname" id="surname" placeholder="Apellidos" value="{{ $values['surname'] }}">
				</div>
				<div class="form-group">
					<label for="email">
						<strong>Email:</strong>
					</label>
					<input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ $values['email'] }}">
				</div>
				<div class="form-group">
					<label for="visits">
						<strong>Visitas:</strong>
					</label>
					<input type="integer" class="form-control" name="visits" id="visits" placeholder="Visitas" value="{{ $values['visits'] }}">
				</div>
				
				<div class="form-check">
					<input class="form-check-input" type="checkbox" name="is_available" id="is_available" @if($values['is_available']) checked="checked" @endif/>
					<label class="form-check-label" for="is_available">
						<strong>Disponible</strong>
					</label>
				</div>

				<div class="form-group mt-3">
					<label for="gender">
						<strong>Género:</strong>
					</label>
					<select class="form-control" name="gender" id="gender">
						<option value="male" @if($values['gender']=='male') selected="selected" @endif>Hombre</option>
						<option value="female" @if($values['gender']=='female') selected="selected" @endif>Mujer</option>
					</select>									
				</div>			
				
				<div class="form-group mt-3">
					<label for="grupo_id">
						<strong>Grupo:</strong>
					</label>
					<select class="form-control" name="grupo_id" id="grupo_id">
						<option value="0">Ninguno</option>
						@foreach($grupos as $grupo)
						<option value="{{ $grupo->id }}" @if($values['grupo_id']==$grupo->id) selected="selected" @endif>{{ $grupo->name }}</option>
						@endforeach
					</select>									
				</div>		

				<div class="form-group mt-3">
					<label for="roles">
						<strong>Roles:</strong>
					</label>
					<select multiple class="form-control" name="roles[]" id="roles">
						@foreach($roles as $rol)
						<option value="{{ $rol->id }}" @if(in_array($rol->id, $values['roles_id'])) selected="selected" @endif>{{ $rol->name }}</option>
						@endforeach
					</select>									
				</div>					
				
				@if(is_numeric($values['id']))
				<div class="form-group">
					<label for="name">
						<strong>Created at:</strong>
					</label>
					<span> {{ $values['created_at'] }}  </span>
				</div>
				<div class="form-group">
					<label for="name">
						<strong>Last update:</strong>
					</label>
					<span> {{ $values['updated_at'] }}  </span>
				</div>  
				@endif
				<button type="submit" class="btn btn-primary">Enviar</button>
			</form>
		</div>
	</div>

	<div class="row mt-2">
		<div class="col">
			<a href="/personas">Volver</a>
		</div>
	</div>	
	
</div>
@stop 
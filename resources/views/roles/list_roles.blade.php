@extends('layouts.main') 

@section('content')
<input type="hidden" id="csrf_token" value="{{ csrf_token() }}"/>
<div class="container mt-2">
	<h1>{{ $title }}</h1>
	<div class="row mt-5">
		<div class="col">
			<a href="/roles/create">Añadir nuevo elemento</a>
		</div>
	</div>
	@if(count($roles)>0)
	<div class="row mt-5">
		<div class="col">
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th scope="col">ID</th>
						@foreach($attributes as $attr)
						<th scope="col">{{ $attr }}</th>
						@endforeach
						<th scope="col">&nbsp;</th>
						<th scope="col">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					@foreach($roles as $rol)
					<tr>
						<th scope="row">
							<a href="/roles/{{ $rol->id }}">{{ $rol->id }}</a>
						</th>
						<td>{{ $rol->name }}</td>
						<td>{{ $rol->created_at }}</td>
						<td>{{ $rol->updated_at }}</td>
						<td>
							<a href="/roles/{{ $rol->id }}/edit">Editar</a>
						</td>
						<td>
							<a href="#" class="delete" data-model="roles" data-id="{{ $rol->id }}">Borrar</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $roles->links() }}	
		</div>
	</div>
	@else
	<div class="row mt-5">
		<div class="col">
			<div class="alert alert-danger alert-dismissible">
				<strong>No hay elementos</strong> Puede añadir un nuevo elemento desde el enlace superior.
			</div>
		</div>

	</div>
	@endif
</div>
@stop 
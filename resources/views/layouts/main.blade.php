<html>
    <head>
        <title>CRUD</title>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
		<script src="/js/main.js"></script>	
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
			@if (Route::has('login'))
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
			  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					@auth
					<li class="nav-item @if($menu_activo=='personas') active @endif">
						<a class="nav-link" href="/personas">Personas</a>
					</li>
					<li class="nav-item @if($menu_activo=='grupos') active @endif">
						<a class="nav-link" href="/grupos">Grupos</a>
					</li>
					<li class="nav-item @if($menu_activo=='roles') active @endif">
						<a class="nav-link" href="/roles">Roles</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();$('#logout-form').submit();">Salir</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
                        </form>
					</li>					
					@else
					<li class="nav-item">
						<a class="nav-link" href="{{ route('login') }}">Login</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{ route('register') }}">Registro</a>
					</li>						
					@endauth				  
				</ul>
			  </div>
			</nav>		
			@endif
			@yield('content')
        </div>
    </body>
</html>
@extends('layouts.main') 

@section('content')
<input type="hidden" id="csrf_token" value="{{ csrf_token() }}"/>
<div class="container mt-2">
	<h1>{{ $title }}</h1>

	<div class="row mt-3">
		<div class="col">	
			<div class="form-group">
				<label for="name">
					<strong>ID:</strong>
				</label>
				<span> {{ $values['id'] }}  </span>
			</div>

			<div class="form-group">
				<label for="name">
					<strong>Nombre:</strong>
				</label>
				<span> {{ $values['name'] }}  </span>
			</div>

			<div class="form-group">
				<label for="description">
					<strong>Descripción:</strong>
				</label>
				<br/>
				<span> {{ $values['description'] }}  </span>
			</div>

			<div class="form-group">
				<label for="name">
					<strong>Personas con este grupo:</strong>
				</label>
				<br/>
				@if(count($values['personas'])>0)
				<ul>
					@foreach($values['personas'] as $persona)
					<li>
						<a href="/personas/{{ $persona->id }}" target="_blank">{{ $persona->name }}</a>
					</li>
					@endforeach
				</ul>
				@else
					<span>No hay personas con este grupo</span>
				@endif
			</div>			
			
			<div class="form-group">
				<label for="name">
					<strong>Created at:</strong>
				</label>
				<span> {{ $values['created_at'] }}  </span>
			</div>

			<div class="form-group">
				<label for="name">
					<strong>Last update:</strong>
				</label>
				<span> {{ $values['updated_at'] }}  </span>
			</div>  
			
		</div>
	</div>
	
	<div class="row mt-2">
		<div class="col">
			<a href="/grupos">Volver</a>
		</div>
	</div>
	
</div>
@stop 
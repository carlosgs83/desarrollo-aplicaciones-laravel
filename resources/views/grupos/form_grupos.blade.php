@extends('layouts.main') 

@section('content')
<div class="container mt-2">
	<h1>{{ $title }}</h1>

	@if ($errors->any())
	<div class="row mt-4">
		<div class="col">	
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	@endif

	<div class="row mt-3">
		<div class="col">	
			<form action="{{ $values['action'] }}" method="post">
				@csrf

				@if(is_numeric($values['id']))
				{{ method_field('PATCH') }}
				@endif

				@if(is_numeric($values['id']))
				<div class="form-group">
					<label for="name">
						<strong>ID:</strong>
					</label>
					<span> {{ $values['id'] }}  </span>
				</div>
				@endif

				<div class="form-group">
					<label for="name">
						<strong>Nombre:</strong>
					</label>
					<input type="text" class="form-control" name="name" id="name" placeholder="Nombre" value="{{ $values['name'] }}">
				</div>
				<div class="form-group">
					<label for="description">
						<strong>Descripción:</strong>
					</label>
					<textarea class="form-control" name="description" id="description" rows="3" placeholder="Descripción">{{ $values['description'] }}</textarea>
				</div>

				@if(is_numeric($values['id']))
				<div class="form-group">
					<label for="name">
						<strong>Created at:</strong>
					</label>
					<span> {{ $values['created_at'] }}  </span>
				</div>
				<div class="form-group">
					<label for="name">
						<strong>Last update:</strong>
					</label>
					<span> {{ $values['updated_at'] }}  </span>
				</div>  
				@endif
				<button type="submit" class="btn btn-primary">Enviar</button>
			</form>
		</div>
	</div>

	<div class="row mt-2">
		<div class="col">
			<a href="/grupos">Volver</a>
		</div>
	</div>	
	
</div>
@stop 
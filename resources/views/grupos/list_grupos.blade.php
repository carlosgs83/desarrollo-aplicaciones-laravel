@extends('layouts.main') 

@section('content')
<input type="hidden" id="csrf_token" value="{{ csrf_token() }}"/>
<div class="container mt-2">
	<h1>{{ $title }}</h1>
	<div class="row mt-5">
		<div class="col">
			<a href="/grupos/create">Añadir nuevo elemento</a>
		</div>
	</div>
	@if(count($grupos)>0)
	<div class="row mt-5">
		<div class="col">
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th scope="col">ID</th>
						@foreach($attributes as $attr)
						<th scope="col">{{ $attr }}</th>
						@endforeach
						<th scope="col">&nbsp;</th>
						<th scope="col">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					@foreach($grupos as $grupo)
					<tr>
						<th scope="row">
							<a href="/grupos/{{ $grupo->id }}">{{ $grupo->id }}</a>
						</th>
						<td>{{ $grupo->name }}</td>
						<td>{{ $grupo->created_at }}</td>
						<td>{{ $grupo->updated_at }}</td>
						<td>
							<a href="/grupos/{{ $grupo->id }}/edit">Editar</a>
						</td>
						<td>
							<a href="#" class="delete" data-model="grupos" data-id="{{ $grupo->id }}">Borrar</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $grupos->links() }}			
		</div>
	</div>
	@else
	<div class="row mt-5">
		<div class="col">
			<div class="alert alert-danger alert-dismissible">
				<strong>No hay elementos</strong> Puede añadir un nuevo elemento desde el enlace superior.
			</div>
		</div>

	</div>
	@endif
</div>
@stop 
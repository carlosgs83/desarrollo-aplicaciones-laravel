<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
	if (Auth::check()) {
		return redirect('login');
	} else {
		return redirect('personas');
	}
});

Route::get('home', function () {
    return redirect('personas');
});

Route::resource('personas', 'PersonasController');
Route::resource('grupos', 'GruposController');
Route::resource('roles', 'RolesController');


/* Creación token API */
Route::get('/create_token', function () {
	$user = Auth::user();
	$token = $user->createToken('token API')->accessToken;
	return $token;
})->middleware('auth');

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Rol;

class RolesController extends Controller
{
	/* Menú: clase activa */
	private $menu_activo = 'roles';	

    /**
     * Protegemos con autenticación el acceso a este controlador
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Vista de listado para roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$roles = Rol::paginate(config('app.pagination'));
		
		$attributes = array(
			'Nombre',
			'Creado',
			'Actualizado'
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,		
					'title' => 'Roles',
					'roles' => $roles,
					'attributes' => $attributes
		);  
		
		return view('roles.list_roles')->with($data);
    }

    /**
     * Mostramos formulario para crear un nuevo item rol.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$values = array(
					'action' => '/roles',
					'id' => null,
					'name' => old('name'),
					'description' => old('description')
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,		
					'title' => 'Crear rol',
					'values' => $values
		);  
		
		return view('roles.form_roles')->with($data);
    }

    /**
     * Guardamos un nuevo item rol.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:200',
            'description' => 'required|string|max:2020',
        ]);

		$name = $request->input('name');
		$description = $request->input('description');
		
		$rol = new Rol();
		$rol->name = $name;
		$rol->description = $description;
		$rol->save();
	
		return redirect(route('roles.index'));
    }

    /**
     * Mostramos el item rol.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$rol = Rol::find($id);

		$values = array(
					'id' => $rol->id,
					'name' => $rol->name,
					'description' => $rol->description,
					'personas' => $rol->personas,
					'created_at' => $rol->created_at,
					'updated_at' => $rol->updated_at
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,
					'title' => 'Rol',
					'values' => $values
		);  
		
		return view('roles.detail_roles')->with($data);
    }

    /**
     * Mostramos formulario para editar un item rol según parámetro ID.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$rol = Rol::find($id);

		$values = array(
					'action' => '/roles/'.$rol->id,
					'id' => $rol->id,
					'name' => $rol->name,
					'description' => $rol->description,
					'created_at' => $rol->created_at,
					'updated_at' => $rol->updated_at
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,					
					'title' => 'Editar rol',
					'values' => $values
		);  
		
		return view('roles.form_roles')->with($data);	
    }

    /**
     * Actualizamos un item rol según parámetro ID.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $request->validate([
            'name' => 'required|string|max:200',
            'description' => 'required|string|max:2020',
        ]);

		$name = $request->input('name');
		$description = $request->input('description');
		
		$rol = Rol::find($id);
		$rol->name = $name;
		$rol->description = $description;
		$rol->save();
	
		return redirect(route('roles.index'));
    }

    /**
     * Eliminamos un item rol según parámetro ID.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$rol = Rol::find($id);
		$rol->delete($id);
		return response()->json([
			'success' => 'Elemento eliminado'
		]);
    }
}

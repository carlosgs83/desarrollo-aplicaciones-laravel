<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Grupo;

class GruposController extends Controller
{
	/* Menú: clase activa */
	private $menu_activo = 'grupos';

    /**
     * Protegemos con autenticación el acceso a este controlador
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Vista de listado para grupos.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$grupos = Grupo::paginate(config('app.pagination'));
		
		$attributes = array(
			'Nombre',
			'Creado',
			'Actualizado'
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,
					'title' => 'Grupos',
					'grupos' => $grupos,
					'attributes' => $attributes
		);  
		
		return view('grupos.list_grupos')->with($data);
    }

    /**
     * Mostramos formulario para crear un nuevo item grupo.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$values = array(
					'action' => '/grupos',
					'id' => null,
					'name' => old('name'),
					'description' => old('description')
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,
					'title' => 'Crear grupo',
					'values' => $values
		);  
		
		return view('grupos.form_grupos')->with($data);
    }

    /**
     * Guardamos un nuevo item grupo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:200',
            'description' => 'required|string|max:2020',
        ]);

		$name = $request->input('name');
		$description = $request->input('description');
		
		$grupo = new Grupo();
		$grupo->name = $name;
		$grupo->description = $description;
		$grupo->save();
	
		return redirect(route('grupos.index'));
    }

    /**
     * Mostramos el item grupo.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$grupo = Grupo::find($id);

		$values = array(
					'id' => $grupo->id,
					'name' => $grupo->name,
					'description' => $grupo->description,
					'personas' => $grupo->personas,
					'created_at' => $grupo->created_at,
					'updated_at' => $grupo->updated_at
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,		
					'title' => 'Grupo',
					'values' => $values
		);  
		
		return view('grupos.detail_grupos')->with($data);	    
	}

    /**
     * Mostramos formulario para editar un item grupo según parámetro ID.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$grupo = Grupo::find($id);

		$values = array(
					'action' => '/grupos/'.$grupo->id,
					'id' => $grupo->id,
					'name' => $grupo->name,
					'description' => $grupo->description,
					'created_at' => $grupo->created_at,
					'updated_at' => $grupo->updated_at
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,
					'title' => 'Editar grupo',
					'values' => $values
		);  
		
		return view('grupos.form_grupos')->with($data);		
    }

    /**
     * Actualizamos un item grupo según parámetro ID.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $request->validate([
            'name' => 'required|string|max:200',
            'description' => 'required|string|max:2020',
        ]);

		$name = $request->input('name');
		$description = $request->input('description');
		
		$grupo = Grupo::find($id);
		$grupo->name = $name;
		$grupo->description = $description;
		$grupo->save();
	
		return redirect(route('grupos.index'));
    }

    /**
     * Eliminamos un item grupo según parámetro ID.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$grupo = Grupo::find($id);
		$grupo->delete($id);
		return response()->json([
			'success' => 'Elemento eliminado'
		]);
    }
}

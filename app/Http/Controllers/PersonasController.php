<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Persona;
use App\Rol;
use App\Grupo;

class PersonasController extends Controller
{
	/* Menú: clase activa */
	private $menu_activo = 'personas';	

    /**
     * Protegemos con autenticación el acceso a este controlador
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Vista de listado para personas.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$personas = Persona::paginate(config('app.pagination'));
		foreach($personas as & $persona) {
			if($persona->grupo_id != null) {
				$persona->grupo_name = $persona->grupo->name;
			}
			else {
				
				$persona->grupo_name = "null";				
			}
		}
		
		$attributes = array(
			'Nombre',
			'Apellidos',
			'Email',
			'Visitas',
			'Disponible',
			'Género',
			'Grupo',
			'Roles',			
			'Creado',
			'Actualizado'
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,		
					'title' => 'Personas',
					'personas' => $personas,
					'attributes' => $attributes
		);  
		
		return view('personas.list_personas')->with($data);
    }

    /**
     * Mostramos formulario para crear un nuevo item persona.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$grupos = Grupo::all();
		$roles = Grupo::all();
		
		$roles_id = old('grupo_id');
		if(!is_array($roles_id)) $roles_id = array();
		
		$values = array(
					'action' => '/personas',
					'id' => null,
					'name' => old('name'),
					'surname' => old('surname'),
					'email' => old('email'),
					'visits' => old('visits'),
					'is_available' => old('is_available'),
					'gender' => old('gender'),
					'grupo_id' => old('grupo_id'),
					'roles_id' => $roles_id
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,
					'title' => 'Crear persona',
					'values' => $values,
					'grupos' => $grupos,
					'roles' => $roles
		);  
		
		return view('personas.form_personas')->with($data);
    }

    /**
     * Guardamos un nuevo item persona.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:200',
            'surname' => 'required|string|max:200',
            'email' => 'required|unique:personas,email|string|max:200',
            'visits' => 'required|integer|min:0',
            'gender' => 'in:male,female'	,
            'grupo_id' => 'required|integer|min:0'	
        ]);

		$name = $request->input('name');
		$surname = $request->input('surname');
		$email = $request->input('email');
		$visits = $request->input('visits');
		if($request->input('is_available') == 'on') {
			$is_available = true;			
		}
		else {
			$is_available = false;
		}
		$gender = $request->input('gender');
		$grupo_id = $request->input('grupo_id');
		/*
			Si no se selecciona grupo, lo mantenemos 
			a null para respetar la integridad referencial
		*/
		if($grupo_id == 0) $grupo_id = null; 		
		
		$persona = new Persona();
		$persona->name = $name;
		$persona->surname = $surname;
		$persona->email = $email;
		$persona->visits = $visits;
		$persona->is_available = $is_available;
		$persona->gender = $gender;
		$persona->grupo_id = $grupo_id;
		
		$persona->save();
	
		return redirect(route('personas.index'));
    }

    /**
     * Mostramos el item persona.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$persona = Persona::find($id);	
		
		if($persona->grupo_id == 0) {
			$grupo_name = "null";
		}
		else {
			$grupo_name = $persona->grupo->name;
		}
		
		$values = array(
					'id' => $persona->id,
					'name' => $persona->name,
					'surname' => $persona->surname,
					'email' => $persona->email,
					'visits' => $persona->visits,
					'is_available' => $persona->is_available,
					'gender' => $persona->gender,
					'grupo_id' => $persona->grupo_id,
					'grupo_name' => $grupo_name,
					'roles' => $persona->roles,
					'created_at' => $persona->created_at,
					'updated_at' => $persona->updated_at
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,
					'title' => 'Editar persona',
					'values' => $values
		);  
		
		return view('personas.detail_personas')->with($data);
    }

    /**
     * Mostramos formulario para editar un item persona según parámetro ID.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$persona = Persona::find($id);	
		$grupos = Grupo::all();
		$roles = Rol::all();
		
		$roles_persona = $persona->roles;
		$roles_id = array();
		foreach($roles_persona as $rp) {
			$roles_id [] = $rp->id;			
		}
		
		$values = array(
					'action' => '/personas/'.$persona->id,
					'id' => $persona->id,
					'name' => $persona->name,
					'surname' => $persona->surname,
					'email' => $persona->email,
					'visits' => $persona->visits,
					'is_available' => $persona->is_available,
					'gender' => $persona->gender,
					'grupo_id' => $persona->grupo_id,
					'roles_id' => $roles_id,				
					'created_at' => $persona->created_at,
					'updated_at' => $persona->updated_at
		);
		
		$data = array(
					'menu_activo' => $this->menu_activo,
					'title' => 'Editar persona',
					'values' => $values,
					'grupos' => $grupos,
					'roles' => $roles
		);  
		
		return view('personas.form_personas')->with($data);
    }

    /**
     * Actualizamos un item persona según parámetro ID.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$persona = Persona::find($id);
		
        $request->validate([
            'name' => 'required|string|max:200',
            'surname' => 'required|string|max:200',
			/* 
				Especificamos que no tengamos en cuenta la 
				comprobación de único para el email del item 
				editado
			*/
            'email' => 'required|unique:personas,email,'.$persona->id.'|string|max:200',			
            'visits' => 'required|integer|min:0',
            'gender' => 'in:male,female',
            'grupo_id' => 'required|integer|min:0',
            'roles.*' => 'int'			
        ]);
		
		$name = $request->input('name');
		$surname = $request->input('surname');
		$email = $request->input('email');
		$visits = $request->input('visits');
		if($request->input('is_available') == 'on') {
			$is_available = true;			
		}
		else {
			$is_available = false;
		}
		$gender = $request->input('gender');
		$grupo_id = $request->input('grupo_id');
		$roles = $request->input('roles');
		
		/*
			Si no se selecciona grupo, lo mantenemos 
			a null para respetar la integridad referencial
		*/
		if($grupo_id == 0) $grupo_id = null; 
		
		$persona = Persona::find($id);
		$persona->name = $name;
		$persona->surname = $surname;
		$persona->email = $email;
		$persona->visits = $visits;
		$persona->is_available = $is_available;
		$persona->gender = $gender;
		$persona->grupo_id = $grupo_id;
		$persona->roles()->sync($roles);
		$persona->save();
		
		return redirect(route('personas.index'));
    }

    /**
     * Eliminamos un item persona según parámetro ID.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$Persona = Persona::find($id);
		$Persona->delete($id);
		return response()->json([
			'success' => 'Elemento eliminado'
		]);
    }
}

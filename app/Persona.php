<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
   /**
     * Relación 1:N con grupos
     */	
	public function grupo() {
		return $this->belongsTo('App\Grupo', 'grupo_id');
	}
	
   /**
     * Relación M:N con roles
     */	
	public function roles() {
		return $this->belongsToMany('App\Rol', 'persona_pivot_rol', 'persona_id', 'rol_id');
	}	
}

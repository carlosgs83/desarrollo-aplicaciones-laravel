<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Persona;

class RandomVisits extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visits:random {min} {max}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza el campo visitas de la tabla personas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$min = $this->argument('min');
		$max = $this->argument('max');
		
		if(!is_numeric($min) || !is_numeric($max))
		{
			$this->error("Los argumentos min y max deben ser numéricos");
			return false;
		}
        $this->line("Actualizando las visitas de la tabla personas...");
		
		$personas = Persona::all();
		
		foreach($personas as $persona)
		{
			$rand = rand($min, $max);
			$this->line("Persona ID ".$persona->id." sumamos nuevas visitas ".$rand);
			$persona->visits = $persona->visits + $rand;
			$persona->save();
		}
				
        $this->info("Visitas actualizadas");
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
   /**
     * Relación N:1 inversa con personas
     */	
	public function personas() {
		return $this->hasMany('App\Persona', 'grupo_id');
	}
}

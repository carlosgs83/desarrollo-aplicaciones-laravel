<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
   /**
     * Tabla asociada con el modelo rol
     *
     * @var string
     */
    protected $table = 'roles';
	
   /**
     * Relación M:N con personas
     */	
	public function personas() {
		return $this->belongsToMany('App\Persona', 'persona_pivot_rol', 'rol_id', 'persona_id');
	}		
}
